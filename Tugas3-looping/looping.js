console.log('\n No.1 Looping While');
console.log('-----------------------------');
console.log('LOOPING PERTAMA')
var deret = 10
var jumlah = 0
while(deret>0){
    jumlah+=2;
    deret--;
    console.log(jumlah+' - I Love Coding');
}
console.log('LOOPING KEDUA')
var deret = 10
var jumlah = 22
while(deret<20){
    jumlah-=2;
    deret++;
    console.log(jumlah+' - I Will Become a Mobile Developer');
}
console.log('\n No.2 Looping Menggunakan For');
console.log('-----------------------------')
for(var i=1;i<=20;i++){
    if((i%2)==0){
        console.log(i +' - Berkualitas');
    }else if((i%2)!==0 && (i%3)==0){
        console.log(i +' - I Love Coding');
    }else if((i%2)!==0){
        console.log(i +' - Santai');
    }
}
console.log('\n No. 3 Membuat Persegi Panjang #');
console.log('-----------------------------')
for (var i = 0; i < 4; i++) {
	var pagar = '';

	for (var j = 0; j < 8; j++) {
			pagar += '#';
	}
	console.log(pagar);
}
console.log('\n No. 4 Membuat Tangga');
console.log('-----------------------------')
for(i=1;i<=1;i++){
    var pagar = '';
    for(j=7;j>=i;j--){
        pagar += '#';
        console.log(pagar);
    }
}
console.log('\n No.5 Membuat Papan Catur');
console.log('-----------------------------')
for (var i = 0; i < 8; i++) {
    if(i%2==0){
        var pagar = ' ';
    }else{
        var pagar = '';
    }

	for (var j = 0; j < 8; j++) {
        if(j%2==0){
            pagar += ' ';
        }else{
            pagar += '#';
        }
	}
	console.log(pagar);
}