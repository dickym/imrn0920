import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, TouchableOpacity, Image} from 'react-native';
import { Fontisto } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

export default class skillscreen extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.navTop}>
                    <Ionicons name="ios-arrow-back" size={35} color="white" />
                </View>
                <View style={styles.logo}>
                    <Fontisto name="react" size={100} color="white" />
                    <Text style={{color:'white',fontSize:20,paddingTop:20}}>React Native Developer</Text>
                    <Text style={{color:'white',fontSize:16,paddingTop:10}}>Dicky Maulana</Text>
                </View>
                <View style={styles.skills}>
                    <Text style={{padding:10,paddingLeft:20,fontSize: 16,fontWeight: 'bold'}}>Skills</Text>
                    <View style={styles.boxSkills}>
                        <Text style={{color:'white',padding:5}}>Programing Language</Text>
                        <View style={styles.barSkills}>
                            <View style={styles.skillLeft}>
                                <View style={{flexDirection:'row'}}>
                                    <MaterialCommunityIcons name="language-javascript" size={40} color="black" />
                                    <Text style={styles.nameSkills}>Javascript</Text>
                                </View>
                                <View style={styles.progressBar}>
                                    <View style={{backgroundColor:'#3dc1d3',margin:5,borderRadius:5,width:75}}>
                                        <Text style={styles.textBar}>50%</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.skillLeft}>
                                <View style={{flexDirection:'row'}}>
                                    <AntDesign name="HTML" size={40} color="black" />
                                    <Text style={styles.nameSkills}>HTML5</Text>
                                </View>
                                <View style={styles.progressBar}>
                                    <View style={{backgroundColor:'#3dc1d3',margin:5,borderRadius:5,width:110}}>
                                        <Text style={styles.textBar}>90%</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#303952',
        height: 500
    },
    navTop:{
        paddingTop: 25,
        paddingLeft: 10
    },
    logo:{
        alignItems: 'center',
        paddingTop: 30
    },
    skills:{
        backgroundColor: 'white',
        height: 400,
        marginTop: 30,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    },
    boxSkills:{
        backgroundColor: '#3dc1d3',
        padding: 10,
        margin: 10,
        borderRadius: 11,
    },
    barSkills:{
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    skillLeft:{
        backgroundColor: '#f5cd79',
        width: 150,
        borderRadius: 5
    },
    nameSkills:{
        color:'#303952',
        textAlignVertical: 'center',
        paddingLeft: 18,
        fontWeight: 'bold'
    },
    progressBar:{
        width:140,
        backgroundColor: 'white',
        margin: 5,
        borderRadius: 5,
    },
    textBar:{
        color: 'black',
        width: 150,
        textAlign: 'center',
        fontWeight: 'bold'
    }
})