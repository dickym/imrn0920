import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, TouchableOpacity, Image} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

export default class aboutScreen extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.navTop}>
                <Ionicons name="ios-arrow-back" size={35} color="white" />
                </View>
                <View style={styles.imgAbout}>
                <Image source={require('./images/stevejobs.jpg')}/>
                </View>
                <View style={styles.aboutProfile}>
                    <Text style={{fontSize:25,fontWeight:"bold",color:"#303952"}}>Dicky Maulana</Text>
                    <Text>React Native Developer</Text>
                    <View style={{flexDirection:"row",marginTop:15}}>
                        <Text>Actively Looking for a Job</Text>
                        <FontAwesome style={{marginLeft:10}} name="check-circle" size={24} color="#3dc1d3" />
                    </View>
                    <View style={{flexDirection:"row",marginTop:20}}>
                        <AntDesign style={{marginRight:10}} name="instagram" size={30} color="black" />
                        <AntDesign name="github" size={28} color="black" />
                    </View>
                </View>
                <View style={styles.completeProfile}>
                    <View>
                    <Text style={{color:"white",fontSize:18}}>Complete Profile</Text>
                    <Text style={{color:"white", fontSize:12,marginTop: 5}}>Skills | Job Experiences</Text>
                    </View>
                    <TouchableOpacity style={styles.buttonProfile}>
                    <AntDesign name="enter" size={24} color="white" />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex:1,
      backgroundColor: 'white',
      
    },
    navTop: {
        padding:20,
        zIndex:1
    },
    imgAbout: {
        position:"absolute"
    },
    aboutProfile:{
        backgroundColor: "white",
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        marginTop: 190,
        padding: 30
    },
    completeProfile:{
        backgroundColor: "#303952",
        height: 400,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        marginTop: 30,
        padding: 25,
        flexDirection: "row",
        justifyContent: "space-between"
    },
    buttonProfile:{
        backgroundColor:"#63cdda",
        height:40,
        width:40,
        justifyContent:"center",
        alignItems: "center",
        borderRadius: 30
    }
})