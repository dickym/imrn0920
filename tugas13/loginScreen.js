import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, TouchableOpacity} from 'react-native';
import { Entypo } from '@expo/vector-icons';

export default class LoginScreen extends Component{
    
    render(){
        
        return(
            <View style={styles.container}>
                <Entypo name="bug" size={100} color="white" />
                <Text style={styles.appName}>SanberApp</Text>
                <View>
                    <TextInput style={styles.textInput} placeholder="Username"/>
                </View>
                <View>
                    <TextInput style={styles.textInput} placeholder="Password"/>
                </View>
                <TouchableOpacity style={styles.loginButton}>
                    <Text style={{textAlign:"center", padding:10,color:"white" }}>
                        Login
                    </Text>
                </TouchableOpacity>
                <View style={{flexDirection:"row",marginTop:20}}>
                    <Text style={{color:"white"}}>Don't Have an Account ?</Text>
                    <Text style={{color:"#63cdda"}}>  Sign Up !</Text>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#303952',
    alignItems: 'center',
    justifyContent: 'center',
  },
  appName: {
      fontSize: 30,
      fontWeight: "bold",
      color: "white"
  },
  textInput: {
      backgroundColor:"white",
      width: 300,
      borderRadius: 5,
      padding: 5,
      fontWeight: "bold",
      marginTop: 20
  },
  loginButton :{
      width:100,
      marginTop: 20,
      backgroundColor: "#63cdda",
      borderRadius:10
  }
});