console.log('\n No.1 Mengubah fungsi menjadi fungsi arrow')
console.log('-----------------------------')
const golden =  goldenFunction = () =>{
    console.log("this is golden!!")
  }
   
  golden()

console.log('\n No.2 Sederhanakan menjadi Object literal di ES6')
console.log('-----------------------------')
const newFunction = literal=(firstName, lastName)=>{
    return {
      fullName: ()=>{
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
  //Driver Code 
  newFunction("William", "Imoh").fullName()

console.log('\n No.3 Destructuring')
console.log('-----------------------------')
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const {firstName,lastName,destination,occupation,spell} = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation)

console.log('\n No.4 Array Spreading')
console.log('-----------------------------')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)

console.log('\n No.5 Template Literals')
console.log('-----------------------------')
const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet, ` +
    `consectetur adipiscing elit, ${planet} do eiusmod tempor` +
    `incididunt ut labore et dolore magna aliqua. Ut enim` +
    `ad minim veniam`
 
// Driver Code
console.log(before)