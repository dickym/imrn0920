import React, { Component }from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons';

export default class splash extends Component{
    render(){
        return(
            <View style={styles.container}>
                <FontAwesome5 name="react" size={100} color="black" />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    }
})