import React, { Component }from 'react'
import { View, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native'
import { Entypo } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

export default class login extends Component{
    render(){
        return(
            <View style={styles.container}>
                <Text style={{fontSize:24,fontWeight:'bold'}}>WELCOME BACK</Text>
                <Text>Sign in to continue</Text>
                <View style={styles.signIn}>
                    <Text>Email</Text>
                    <TextInput style={styles.textInput} placeholder="email"/>
                    <Text>Password</Text>
                    <TextInput style={styles.textInput} placeholder="password"/>
                    <Text style={{textAlign:'right'}}>Forgot Password?</Text>
                    <TouchableOpacity style={styles.signBtn}>
                        <Text style={styles.signText}>Sign In</Text>
                    </TouchableOpacity>
                    <Text style={{textAlign:'center'}}>-OR-</Text>
                    <View style={styles.loginMedsos}>
                        <View style={styles.medsosBtn}>
                            <Entypo name="facebook" size={20} color="black" />
                            <Text style={{padding:1,marginLeft:10}}>Facebook</Text>
                        </View>
                        <View style={styles.medsosBtn}>
                            <AntDesign name="google" size={20} color="black" />
                            <Text style={{padding:1,marginLeft:10}}>Google</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        padding:24
    },
    signIn:{
        marginTop:20,
        padding:20,
        borderWidth: 0.3,
        borderColor: "#596275",
        borderRadius: 20,
    },
    textInput:{
        borderBottomWidth: 0.3,
        paddingTop: 5,
        marginBottom: 20
    },
    signBtn:{
        backgroundColor: '#F77866',
        borderRadius: 5,
        marginTop: 30,
        marginBottom: 30
    },
    signText:{
        color: 'white',
        lineHeight:36,
        textAlign: 'center',
    },
    medsosBtn:{
        flexDirection: 'row',
        borderWidth: 0.3,
        padding:10,
        borderRadius: 3,
        marginTop: 20
    },
    loginMedsos:{
        flexDirection: 'row',
        justifyContent: 'space-around',
    }
})