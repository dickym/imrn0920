import React, { Component }from 'react'
import { View, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native'


export default class login extends Component{
    render(){
        return(
            <View style={styles.container}>
                <Text style={{fontSize:24,fontWeight:'bold'}}>WELCOME</Text>
                <Text>Sign up to continue</Text>
                <View style={styles.signIn}>
                    <Text>Name</Text>
                    <TextInput style={styles.textInput} placeholder="name"/>
                    <Text>Email</Text>
                    <TextInput style={styles.textInput} placeholder="email"/>
                    <Text>Phone Number</Text>
                    <TextInput style={styles.textInput} placeholder="Phone Number"/>
                    <Text>Password</Text>
                    <TextInput style={styles.textInput} placeholder="Password"/>
                    <TouchableOpacity style={styles.signBtn}>
                        <Text style={styles.signText}>Sign Up</Text>
                    </TouchableOpacity>
                    <View style={{flexDirection: 'row',justifyContent:'center'}}>
                        <Text style={{fontSize: 12}}>Already have an account ?</Text>
                        <Text style={{fontSize: 12,color:'#F77866'}}> Sign in</Text>
                    </View>
                    
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        padding:24
    },
    signIn:{
        marginTop:20,
        padding:20,
        borderWidth: 0.3,
        borderColor: "#596275",
        borderRadius: 20,
    },
    textInput:{
        borderBottomWidth: 0.3,
        paddingTop: 5,
        marginBottom: 20
    },
    signBtn:{
        backgroundColor: '#F77866',
        borderRadius: 5,
        marginTop: 30,
        marginBottom: 10
    },
    signText:{
        color: 'white',
        lineHeight:36,
        textAlign: 'center',
    },
    medsosBtn:{
        flexDirection: 'row',
        borderWidth: 0.3,
        padding:10,
        borderRadius: 3,
        marginTop: 20
    },
    loginMedsos:{
        flexDirection: 'row',
        justifyContent: 'space-around',
    }
})