import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, TextInput, ScrollView } from 'react-native'
import { EvilIcons } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';



export default class homescreen extends Component{
    render(){
        return(
            <View style={styles.container}>
                <ScrollView>
                <View style={styles.navBarTop}>
                    <View style={styles.searchBar}>
                        <EvilIcons style={styles.searchIcon} name="search" size={24} color="black" />
                        <TextInput style={styles.textInput} placeholder="Search Product"/>
                        <Feather style={styles.cam} name="camera" size={19} color="#727C8E" />
                    </View>
                    <View>
                        <FontAwesome style={styles.bell} name="bell-o" size={19} color="#727C8E" />
                    </View>
                </View>
                <View style={styles.homeImg}>
                    <Image source={require('../assets/img.jpg')} style={{width:330, height:150,borderRadius:5}} />
                </View>
                <View style={styles.category}>
                    <View>
                        <FontAwesome5 style={styles.iconMan} name="tshirt" size={20} color="white" />  
                        <Text style={styles.textCategory}>Man</Text>
                    </View>
                    <View>
                        <Ionicons style={styles.iconWoman} name="ios-woman" size={30} color="white" />
                        <Text style={styles.textCategory}>Woman</Text>
                    </View>
                    <View>
                        <AntDesign style={styles.iconKids} name="meh" size={22} color="white" />
                        <Text style={styles.textCategory}>Kids</Text>
                    </View>
                    <View>
                    <FontAwesome style={styles.iconHome} name="home" size={24} color="white" />
                        <Text style={styles.textCategory}>Home</Text>
                    </View>
                    <View>
                        <AntDesign style={styles.iconMore} name="right" size={24} color="white" />
                        <Text style={styles.textCategory}>More</Text>
                    </View>
                </View>
                <View>
                    <View style={styles.productBox}>
                        <Text style={styles.productTitle}>Flash Sale</Text>
                        <TouchableOpacity>
                            <Text>ALL</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.productDetail}>
                        <View style={styles.boxProduct}>
                            <Image stlye={styles.imgProduct} source={require('../assets/download.jpeg')} style={{width:100,height:100,borderTopLeftRadius:10,borderTopRightRadius:10}}/>
                            <Text style={styles.nameProduct}>iPhone 7</Text>
                            <Text style={styles.priceproduct}>IDR 8.000.000</Text>
                        </View>
                        <View style={styles.boxProduct}>
                        <Image stlye={styles.imgProduct} source={require('../assets/download.jpeg')} style={{width:100,height:100,borderTopLeftRadius:10,borderTopRightRadius:10}}/>
                            <Text style={styles.nameProduct}>iPhone 7 Second</Text>
                            <Text style={styles.priceproduct}>IDR 4.000.000</Text>
                        </View>
                        <View style={styles.boxProduct}>
                        <Image stlye={styles.imgProduct} source={require('../assets/download.jpeg')} style={{width:100,height:100,borderTopLeftRadius:10,borderTopRightRadius:10}}/>
                            <Text style={styles.nameProduct}>iPhone 7+</Text>
                            <Text style={styles.priceproduct}>IDR 9.000.000</Text>
                        </View>
                    </View>
                </View>
                <View>
                    <View style={styles.productBox}>
                        <Text style={styles.productTitle}>New Product</Text>
                        <TouchableOpacity>
                            <Text>ALL</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.productDetail}>
                        <View style={styles.boxProduct}>
                            <Image stlye={styles.imgProduct} source={require('../assets/download.jpeg')} style={{width:150,height:100,borderTopLeftRadius:10,borderTopRightRadius:10}}/>
                            <Text style={styles.nameProduct}>iPhone 7</Text>
                            <Text style={styles.priceproduct}>IDR 8.000.000</Text>
                        </View>
                        <View style={styles.boxProduct}>
                        <Image stlye={styles.imgProduct} source={require('../assets/download.jpeg')} style={{width:150,height:100,borderTopLeftRadius:10,borderTopRightRadius:10}}/>
                            <Text style={styles.nameProduct}>iPhone 7 Second</Text>
                            <Text style={styles.priceproduct}>IDR 4.000.000</Text>
                        </View>
                    </View>
                </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding: 15
    },
    navBarTop:{
        flexDirection: 'row',
        justifyContent:'space-between',
    },
    searchBar:{
        flexDirection:'row',
        padding: 6,
        borderWidth: 0.3,
        borderRadius: 8
    },
    textInput:{
        width:190,
        borderRightWidth:0.3,
        marginRight:10,
        marginLeft: 20
    },
    searchIcon:{
        textAlignVertical: 'center',
        marginLeft: 20
    },
    cam:{
        textAlignVertical: 'center',
        marginRight:5
    },
    bell:{
        lineHeight: 40
    },
    homeImg:{
        marginTop: 13
    },
    category:{
        flexDirection:'row',
        justifyContent: 'space-between',
        marginTop: 14
    },
    textCategory:{
        textAlign: 'center'
    },
    iconMan:{
        backgroundColor: '#FF7171',
        width:50,
        padding:10,
        borderRadius: 15,
        textAlign: 'center',
        height:50,
        textAlignVertical:'center'
    },
    iconWoman:{
        backgroundColor: '#A2F0E6',
        padding:5,
        borderRadius:15 ,
        textAlign: 'center',
        width:50,
        height:50,
        textAlignVertical:'center'
    },
    iconKids:{
        backgroundColor: '#7AD0FF',
        padding:10,
        borderRadius:15,
        width:50,
        textAlign:'center',
        height:50,
        textAlignVertical:'center'
    },
    iconHome:{
        backgroundColor:'#CCAAFF',
        padding:9,
        borderRadius:15,
        height:50,
        width:50,
        textAlign:'center',
        textAlignVertical:'center'
    },
    iconMore:{
        backgroundColor:'#505862',
        padding:9,
        borderRadius:15,
        height:50,
        width:50,
        textAlign:'center',
        textAlignVertical:'center'
    },
    productBox:{
        flexDirection:'row',
        justifyContent: 'space-between',
        marginTop: 20,
        marginBottom: 10,
    },
    productTitle:{
        fontSize:20
    },
    productDetail:{
        flexDirection:'row',
        justifyContent:'space-around',
    },
    imgProduct:{
        borderTopLeftRadius: 5
    },
    nameProduct:{
        fontSize: 12,
        padding: 5
    },
    priceproduct:{
        fontSize: 12,
        fontWeight: 'bold',
        paddingLeft:6,
        marginBottom: 10
    },
    boxProduct:{
        borderWidth: 0.3,
        borderRadius: 5
    }

})