import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/videoItem';
import data from './data.json';


export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image source={require('./images/logo.png')} style={{width:98,height:22}} />
          <View style={styles.rightNav}>
            <TouchableOpacity>
            <Icon style={styles.navItem} name="search" size={25}></Icon>
            </TouchableOpacity>
            <TouchableOpacity>
            <Icon style={styles.navItem} name="account-circle" size={25}></Icon>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.body}>
          <FlatList
            data={data.items}
            renderItem={(video) =><VideoItem video={video.item} />}
            keyExtractor={(item)=>item.id}
            itemSeparatorComponent={()=><View style={{height:0.5, backgroundColor: "#5e5e5e"}}/>}
          />
        </View>
        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name='home' size={25}/>
            <Text style={styles.tabTitel}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name='whatshot' size={25}/>
            <Text style={styles.tabTitel}>Trending</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name='subscriptions' size={25}/>
            <Text style={styles.tabTitel}>subscription</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name='folder' size={25}/>
            <Text style={styles.tabTitel}>Library</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20
  },
  navBar:{
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  rightNav:{
    flexDirection: 'row'
  },
  navItem:{
    marginLeft: 25
  },
  body:{
    flex: 1
  },
  tabBar:{
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#555555',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  tabTitle:{
    fontSize:11,
    color: '#3c3c3c',
  },
  tabItem:{
    alignItems: 'center',
    justifyContent: 'center',
  }
});











