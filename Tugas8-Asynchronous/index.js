// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var i = 0
function buku(durasi, daftarBuku){
    if(daftarBuku[i] != undefined){
        readBooks(durasi, daftarBuku[i], function(sisaDurasi){
            i++
            buku(sisaDurasi, books)
        })
    }
}
buku(10000,books)
