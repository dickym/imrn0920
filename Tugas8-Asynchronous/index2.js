const readBooks = require('./callback.js');
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var i = 0
function buku(durasi, daftarBuku){
    if(daftarBuku[i] != undefined){
        readBooksPromise(durasi, daftarBuku[i], function(sisaDurasi){
            i++
            buku(sisaDurasi, books)
        })
    }
}
buku(10000,books)