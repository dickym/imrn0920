console.log('\n No.1 (Range)')
console.log('==================')
function range(startNum, finishNum){
    let array = [];
if(startNum == undefined || finishNum == undefined){
    array = -1;
}
    for(let i = startNum; i <= finishNum; i ++){
       array.push(i);
    }
    for(let i = startNum;i >= finishNum;i--){
        array.push(i);
    }
    return array;   
}
console.log(range(1, 10));    
console.log(range(1));
console.log(range(11, 18));
console.log(range(54,50));
console.log(range()); 

console.log('\n No.2 (Range With Step)')
console.log('==================')
function rangeWithStep(startNum, finishNum,step){
    let array = [];

if(startNum == undefined){
    array = -1;
}else if(startNum == 1 && finishNum == undefined){
    array = 1;
}
    for(let i = startNum; i <= finishNum; i ++){
       array.push(i);
       i+=step-1;
    }
    for(let i = startNum;i >= finishNum;i--){
        array.push(i);
        i-=step-1;
    }
    return array;   
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

console.log('\n No.3 (Sum Of Range)')
console.log('==================')
function rangeWithStep(startNum, finishNum,step){
    let array = [];

    step = step || 1
    finishNum = finishNum || 0
    for(let i = startNum; i <= finishNum; i ++){
        array.push(i);
        i+=step-1;
    }
    for(let i = startNum;i >= finishNum;i--){
        array.push(i);
        i-=step-1;
    }
    return array;   
}
  
  function sum(array) {
    if(array.length === 0)
      return 0;
    return array.pop() + sum(array);
  }
  
console.log(sum(rangeWithStep(1,10))) // 55
console.log(sum(rangeWithStep(5, 50, 2))) // 621
console.log(sum(rangeWithStep(15,10))) // 75
console.log(sum(rangeWithStep(20, 10, 2))) // 90
console.log(sum(rangeWithStep(1))) // 1
console.log(sum(rangeWithStep())) // 0

console.log('\n No. 4 (Array Multidimensi)')
console.log('==================')
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
var proses1 = 0;
var proses2 = input.length;
var output = '';

function dataHandling (input){
for(proses1; proses1 < proses2; proses1++){
output += 'Nomor ID: ' + input [proses1] [0] + '\n' +
  'Nama Lengkap: ' + input [proses1] [1] + '\n' +
  'TTL: ' + input [proses1] . slice (2,4) . join (' ') + '\n' +
  'Hobi: ' + input [proses1] [4] + '\n' + '\n';
}return output;
}
console.log(dataHandling (input))


console.log('\n No. 5 (Balik Kata)')
console.log('==================')
function balikKata(kata){
    return kata.split("").reverse().join("");
} 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 



console.log('\n No. 6 (Metode Array)')
console.log('==================')
function dataHandling2() {
    input.splice(4,1, 'Pria', 'SMA Internasional Metro');
    console.log(input);

    var tanggal = input[3].split('/');
    var bulan = tanggal.slice(1,2);
    if (bulan==01){
      bulan='Januari';
    } else if (bulan==02){
      bulan='Februari';
    } else if (bulan==03){
      bulan='Maret';
    } else if (bulan==04){
      bulan='April';
    } else if (bulan==05){
      bulan='Mei';
    } else if (bulan==06){
      bulan='Juni';
    } else if (bulan==07){
      bulan='Juli';
    } else if (bulan==08){
      bulan='Agustus';
    } else if (bulan==09){
      bulan='September';
    } else if (bulan==10){
      bulan='Oktober';
    } else if (bulan==11){
      bulan='November';
    } else {
      bulan='Desember';
    }
    console.log(bulan);

    tanggal.sort(function(value1, value2) { return value2 - value1;});
    console.log(tanggal);

    var formatTanggal = tanggal.join('-');
    console.log(formatTanggal);

    var nama = input[1].slice(0,15);
    console.log(nama);
  }

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);