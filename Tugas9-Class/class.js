console.log('\n No.1 Animal Class - Release 1&2')
console.log('-----------------------------')
class Animal {
    constructor(name) {
      this.legs = 4;
      this.cold_blooded = false;
      this.name = name;
    }
    get name() {
      return this._name;
    }
    set name(x) {
      this._name = x;
    }
}

class Frog extends Animal{
    constructor(name){
      super(name);
      this.Frog = name;   
    }
    jump(){
        return "Hop Hop"
    }
}
class Ape {
    constructor(name){
        this.Ape = name;
    }
    yell(){
        return "Auooo"
    }
}
sungokong = new Ape("Kera Sakti")
kodok = new Frog("buduk");
sheep = new Animal("Shaun");

console.log(sheep.name); 
console.log(sheep.legs); 
console.log(sheep.cold_blooded); 
console.log(kodok.jump());
console.log(sungokong.yell());

console.log('\n No.2 Function to Class')
console.log('-----------------------------')
class Clock {
    constructor({ template }) {
      this.template = template;
    }
  
    render() {
      let date = new Date();
  
      let hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      let mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      let secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
  }
var clock = new Clock({template: 'h:m:s'});
clock.start(); 